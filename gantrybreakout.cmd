# Pin name action command file

# Start of element CONN7
ChangePinName(CONN7, 6, gnd)
ChangePinName(CONN7, 5, Vin_Vdc)
ChangePinName(CONN7, 4, HeatRtn)
ChangePinName(CONN7, 3, Heat1)
ChangePinName(CONN7, 2, HeatRtn)
ChangePinName(CONN7, 1, Heat0)

# Start of element CONN6
ChangePinName(CONN6, 6, gnd)
ChangePinName(CONN6, 5, Vin_Vdc)
ChangePinName(CONN6, 4, HeatRtn)
ChangePinName(CONN6, 3, Heat1)
ChangePinName(CONN6, 2, HeatRtn)
ChangePinName(CONN6, 1, Heat0)

# Start of element CONN5
ChangePinName(CONN5, 20, fan0)
ChangePinName(CONN5, 19, fan0_1)
ChangePinName(CONN5, 18, aux0-a)
ChangePinName(CONN5, 17, therm1)
ChangePinName(CONN5, 16, therm0)
ChangePinName(CONN5, 15, gnd)
ChangePinName(CONN5, 14, fan1_1)
ChangePinName(CONN5, 13, aux0-b)
ChangePinName(CONN5, 12, fan1)
ChangePinName(CONN5, 11, 5v_vcc)
ChangePinName(CONN5, 10, gnd)
ChangePinName(CONN5, 9, E1M_2B)
ChangePinName(CONN5, 8, E1M_2A)
ChangePinName(CONN5, 7, E1M_1A)
ChangePinName(CONN5, 6, E1M_1B)
ChangePinName(CONN5, 5, gnd)
ChangePinName(CONN5, 4, E0M_1B)
ChangePinName(CONN5, 3, E0M_1A)
ChangePinName(CONN5, 2, E0M_2A)
ChangePinName(CONN5, 1, E0M_2B)

# Start of element CONN4
ChangePinName(CONN4, 26, 5v_vcc)
ChangePinName(CONN4, 25, aux0-b)
ChangePinName(CONN4, 24, aux0-a)
ChangePinName(CONN4, 23, therm0)
ChangePinName(CONN4, 22, fan0)
ChangePinName(CONN4, 21, gnd)
ChangePinName(CONN4, 20, fan0_1)
ChangePinName(CONN4, 19, fan1_1)
ChangePinName(CONN4, 18, therm1)
ChangePinName(CONN4, 17, fan1)
ChangePinName(CONN4, 16, x_endstop)
ChangePinName(CONN4, 15, gnd)
ChangePinName(CONN4, 14, XM_1B)
ChangePinName(CONN4, 13, XM_1A)
ChangePinName(CONN4, 12, XM_2A)
ChangePinName(CONN4, 11, XM_2B)
ChangePinName(CONN4, 10, gnd)
ChangePinName(CONN4, 9, E1M_1B)
ChangePinName(CONN4, 8, E1M_1A)
ChangePinName(CONN4, 7, E1M_2A)
ChangePinName(CONN4, 6, E1M_2B)
ChangePinName(CONN4, 5, gnd)
ChangePinName(CONN4, 4, E0M_1B)
ChangePinName(CONN4, 3, E0M_1A)
ChangePinName(CONN4, 2, E0M_2A)
ChangePinName(CONN4, 1, E0M_2B)

# Start of element CONN3
ChangePinName(CONN3, 4, 4)
ChangePinName(CONN3, 3, 3)
ChangePinName(CONN3, 2, 2)
ChangePinName(CONN3, 1, 1)

# Start of element CONN2
ChangePinName(CONN2, 2, 2)
ChangePinName(CONN2, 1, 1)

# Start of element CONN1
ChangePinName(CONN1, 4, 4)
ChangePinName(CONN1, 3, 3)
ChangePinName(CONN1, 2, 2)
ChangePinName(CONN1, 1, 1)
