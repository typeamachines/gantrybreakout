Gantry distribution breakout PCB 
----------------------------------
Designed for Series 1 2014 Model, using geda:gaf 

This distribution pcb connects signals from the Etray pcb distirbution board, connects the endstop and X motors, and sends them out to carriage pcb and carriage pcb via 6 pin 24AWG and 20 conductor high density ribbon cable.

Refer to semver.org for versioning information. As far as I know, symantic versioning hasn't been used for HW before, dependancies will be added as they become clear in the master overview of the systems integrated with this board. 
